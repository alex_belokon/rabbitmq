﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Services;
using RabbitMQ.Wrapper.QueueServices;
using RabbitMQ.Client;
using ConnectionFactory = RabbitMQ.Wrapper.QueueServices.ConnectionFactory;

namespace Ponger
{
    class Program
    {
        private static IServiceProvider _serviceProvider;
        public static IConfigurationRoot configuration;

        static void Main(string[] args)
        {
            RegisterServices();
            IServiceScope scope = _serviceProvider.CreateScope();
            scope.ServiceProvider.GetRequiredService<ConsoleApplication>().Run();
            DisposeServices();

        }

        private static void RegisterServices()
        {
            configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false)
                .Build();
            var services = new ServiceCollection();
            services.AddSingleton<IConfigurationRoot>(configuration);
            services.AddTransient<IMessageService, MessageService>();

            services.AddScoped<IMessageQueue, MessageQueue>();
            services.AddSingleton<IConnectionFactory>(x => new ConnectionFactory(new Uri(configuration.GetSection("Rabbit").Value)));

            services.AddScoped<IMessageProducer, MessageProducer>();
            services.AddScoped<IMessageProducerScope, MessageProducerScope>();
            services.AddSingleton<IMessageProducerScopeFactory, MessageProducerScopeFactory>();

            services.AddScoped<IMessageConsumer, MessageConsumer>();
            services.AddScoped<IMessageConsumerScope, MessageConsumerScope>();
            services.AddSingleton<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>();

            services.AddSingleton<ConsoleApplication>();
            _serviceProvider = services.BuildServiceProvider(true);
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }
    }
}

