﻿using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Constants;
using RabbitMQ.Wrapper.Interfaces;
using System;
using System.Text;
using System.Threading;

namespace Ponger
{
    class ConsoleApplication
    {
        private readonly IMessageService _messageService;
        public ConsoleApplication(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public void Run()
        {
            Console.WriteLine("Ponger Started!");
            _messageService.ListenQueue += GetValue;
            Console.ReadLine();

        }

        private void GetValue(object sender, BasicDeliverEventArgs args)
        {

            var processed = false;
            try
            {
                var value = Encoding.UTF8.GetString(args.Body);
                if (value.ToLower() == Constants.PING)
                {
                    Console.WriteLine($"Time: '{DateTime.Now}' Message: '{value}'");
                    Thread.Sleep(2500);
                    _messageService.SendMessageToQueue(Constants.PONG);
                }

                processed = true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                processed = false;
            }
            finally
            {
                _messageService.SetAcknowledge(args, processed);
            }

        }
    }
}
