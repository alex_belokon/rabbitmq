﻿using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Constants;
using RabbitMQ.Wrapper.Interfaces;
using System;
using System.Text;
using System.Threading;

namespace Pinger
{
    class ConsoleApplication
    {
        private readonly IQueueService _queueservice;
        public ConsoleApplication(IQueueService queueservice)
        {
            _queueservice = queueservice;
        }

        public void Run()
        {
            Console.WriteLine("Pinger Started!");
            if (_queueservice.SendMessageToQueue(Constants.PING))
            {
                _queueservice.ListenQueue += GetValue;
            }
            else
            {
                Console.WriteLine("Unable to send message to queue");
            }
            
            Console.ReadLine();
        }

        private void GetValue(object sender, BasicDeliverEventArgs args)
        {
            var value = Encoding.UTF8.GetString(args.Body);
            _queueservice.SetAcknowledge(args);
            Console.WriteLine($"Time: '{DateTime.Now}' Message: '{value}'");
            if (value.ToLower() == Constants.PONG)
            {
                Thread.Sleep(2500);
                _queueservice.SendMessageToQueue(Constants.PING);
            }
        }
    }
}
