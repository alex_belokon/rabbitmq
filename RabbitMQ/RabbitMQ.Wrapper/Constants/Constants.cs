﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.Constants
{
    public static class Constants
    {
        public static string PING = "ping";
        public static string PONG = "pong";
    }
}
