﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.Services
{
    public class QueueService : IQueueService
    {
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IMessageConsumerScope _messageConsumerScope;

        public event EventHandler<BasicDeliverEventArgs> ListenQueue;


        public QueueService(IMessageProducerScopeFactory messageProducerScopeFactory, IMessageConsumerScopeFactory messageConsumerScopeFactory)
        {
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "PongExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "pong_queue",
                RoutingKey = "topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "PingExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "ping_queue",
                RoutingKey = "ping"
            });


            _messageConsumerScope.MessageConsumer.Received += GetValue;
        }

        
        public bool SendMessageToQueue(string value)
        {
            try
            {
                _messageProducerScope.MessageProducer.Send(value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public void SetAcknowledge(BasicDeliverEventArgs args)
        {
            _messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, true);
        }
        private void GetValue(object sender, BasicDeliverEventArgs args)
        {
            ListenQueue?.Invoke(sender, args);
        }

        public void Dispose()
        {
            _messageConsumerScope.Dispose();
            _messageProducerScope.Dispose();
        }
    }
}
