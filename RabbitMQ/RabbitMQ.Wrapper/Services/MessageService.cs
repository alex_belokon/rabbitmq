﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.Services
{
    public class MessageService : IMessageService
    {
        private readonly IMessageConsumerScope _messageConsumerScope;
        private readonly IMessageProducerScope _messageProducerScope;

        public event EventHandler<BasicDeliverEventArgs> ListenQueue;

        public MessageService(IMessageProducerScopeFactory messageProducerScopeFactory, IMessageConsumerScopeFactory messageConsumerScopeFactory)
        {
            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "PongExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "pong_queue",
                RoutingKey = "*.queue.#"
            });

            _messageConsumerScope.MessageConsumer.Received += GetReceived;

            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "PingExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "ping_queue",
                RoutingKey = "ping"
            });
        }

        private void GetReceived(object sender, BasicDeliverEventArgs args)
        {
            ListenQueue?.Invoke(sender, args);
        }

        public void SendMessageToQueue(string receivedValue)
        {
            _messageProducerScope.MessageProducer.Send(receivedValue);
        }

        public void SetAcknowledge(BasicDeliverEventArgs args, bool processed)
        {
            _messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, processed);
        }

        public void Dispose()
        {
            _messageConsumerScope.Dispose();
            _messageProducerScope.Dispose();
        }
    }
}
