﻿using RabbitMQ.Client.Events;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumer
    {
        void Connect();
        void SetAcknowledge(ulong deliveryTag, bool processed);

        event EventHandler<BasicDeliverEventArgs> Received;
    }
}
