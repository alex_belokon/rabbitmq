﻿using System;
using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageQueue : IDisposable
    {
        void DeclareExchange(string exchangeName, string exchangeType);
        void BindQueue(string exchangeName, string routingKey, string queueName);

        IModel Channel { get; set; }
    }
}
