﻿using System;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageService : IDisposable
    {
        void SendMessageToQueue(string receivedValue);
        event EventHandler<BasicDeliverEventArgs> ListenQueue;
        void SetAcknowledge(BasicDeliverEventArgs args, bool processed);
    }
}
