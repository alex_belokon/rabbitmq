﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducer
    {
        void Send(string message, string type = null);
        void SendTyped(Type type, string message);
    }
}
