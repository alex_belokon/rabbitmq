﻿using RabbitMQ.Client.Events;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IQueueService : IDisposable
    {
        bool SendMessageToQueue(string value);
        event EventHandler<BasicDeliverEventArgs> ListenQueue;
        void SetAcknowledge(BasicDeliverEventArgs args);
    }
}
